# a-douglas conda recipe

Home: "https://gitlab.esss.lu.se/nice/conda-bot-testbed/testbed-modules/a-douglas"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS a-douglas module
